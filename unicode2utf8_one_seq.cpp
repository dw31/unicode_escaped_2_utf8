// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   unicode2utf8_one_seq.cpp                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: wide-aze <wide-aze@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2016/01/07 10:03:45 by wide-aze          #+#    #+#             //
//   Updated: 2016/01/07 11:46:05 by wide-aze         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

/*
** c++ function to convert escaped unicode string (like "probl\u00e8me") to utf8 string
** /!\ this one convert one sequence
** (take the sequence as string like "\u00e8" + a tab[4] which is filled and returned)
**
** can be used with replace like 
** str.replace(1, 6, unicode2utf8_one_seq(&tmptab[0], str.substr(1+2, 6)));
** string was "s\\u00e9rieux" and become "sérieux"
*/

char	*unicode2utf8_one_seq(char *tmptab, const string &str)
{
	istringstream	iss(str);
	int				cur, count;

	tmptab[0] = 0;
	tmptab[1] = 0;
	tmptab[2] = 0;
	tmptab[3] = 0;
	if (iss >> hex >> cur)
	{
		if (cur < 0x0080)
			count = 1;
		else if (cur < 0x0800)
			count = 2;
		else if (cur < 0x10000)
			count = 3;
		else if (cur <= 0x10FFFF)
			count = 4;
		else
			return (tmptab);
		if (count > 1)
		{
			for (int i = count - 1; i > 0; --i)
			{
				tmptab[i] = (char)(0x80 | (cur & 0x3F));
				cur >>= 6;
			}
			for (int i = 0; i < count; ++i)
				cur |= (1 << (7 - i));
		}
	tmptab[0] = (char)cur;
	}
	return (tmptab);
}
