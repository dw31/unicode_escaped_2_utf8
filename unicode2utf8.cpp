// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   unicode2utf8.cpp                                   :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: wide-aze <wide-aze@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2016/01/06 14:03:45 by wide-aze          #+#    #+#             //
//   Updated: 2016/01/06 22:46:05 by wide-aze         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

/*
** c++ function to convert escaped unicode string (like "probl\u00e8me") to utf8 string
*/

#include <string>
#include <sstream>

using namespace std;

#define UNICODE2UTF8_16 "0123456789abcdefABCDEF"

static string	unicode2utf8_2(int cur)
{
	string		ret;
	int			count;

	if (cur < 0x0080)
		count = 1;
	else if (cur < 0x0800)
		count = 2;
	else if (cur < 0x10000)
		count = 3;
	else if (cur <= 0x10FFFF)
		count = 4;
	else
		return (ret);
	ret.resize(count);
	if (count > 1)
	{
		for (int i = count - 1; i > 0; --i)
		{
			tmptab[i] = (char)(0x80 | (cur & 0x3F));
			cur >>= 6;
		}
		for (int i = 0; i < count; ++i)
			cur |= (1 << (7 - i));
	}
	ret[0] = (char)cur;
	return (ret);
}

void			unicode2utf8(string &str)
{
	size_t		startIdx = 0, endIdx = 0;
	string		utf8;

	while (42)
	{
		if ((startIdx = str.find("\\u", startIdx)) == string::npos
		|| (endIdx = str.find_first_not_of(UNICODE2UTF8_16, startIdx + 2)) == string::npos)
			return ;
		istringstream iss(str.substr(startIdx + 2, endIdx - (startIdx + 2)));
		int cur;
		if (iss >> hex >> cur)
		{
			utf8 = unicode2utf8_2(cur);
			str.replace(startIdx, 2 + endIdx - (startIdx + 2), utf8);
			startIdx += utf8.length();
		}
		else
			startIdx += 2;
	}
}
